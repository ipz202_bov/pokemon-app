import axios, {AxiosResponse} from 'axios';
import {createAsyncThunk, createSlice, PayloadAction} from "@reduxjs/toolkit";

import {PokemonItemType, PokemonItemTypeDetails, PokemonType} from "../../types/pokemon";

interface PokemonState {
  pokemon: PokemonType,
  status: 'idle' | 'pending' | 'fulfilled' | 'rejected',
  selectedPokemonItem: PokemonItemTypeDetails | null,
  error: string | null,
}

const initialState: PokemonState = {
  pokemon: {
    count: null,
    page: 1,
    previous: null,
    current: `${import.meta.env.VITE_REACT_API_URL}/?offset=0&limit=${import.meta.env.VITE_POKEMON_COUNT_LIMIT}`,
    next: null,
    pokemon: [],
  },
  selectedPokemonItem: null,
  status: 'idle',
  error: null,
}

export const getAll = createAsyncThunk(
  "pokemon/getAll",
  async ({url, page}, {rejectWithValue}) => {
    try {
      const response: AxiosResponse = await axios.get(url);

      if (!response || response.status >= 400) {
        throw new Error('Server error. Unable to fetch pokemon data');
      }

      const pokemonList: PokemonItemType[] = [];

      for (const item of response.data.results) {
        const itemResponse = await axios.get(item.url);

        if (!itemResponse || itemResponse.status >= 400) {
          throw new Error('Server error. Unable to fetch pokemon data');
        }

        const types = [];
        for (const type of itemResponse.data.types) {
          types.push(type.type.name);
        }

        pokemonList.push({
          id: itemResponse.data.id,
          name: item.name,
          image: itemResponse.data.sprites.front_default,
          experience: itemResponse.data.base_experience,
          types
        });
      }

      const pokemon: PokemonType = {
        count: response.data.count,
        page: page || 1,
        previous: response.data.previous,
        current: url,
        next: response.data.next,
        pokemon: pokemonList
      };

      return pokemon;
    } catch (error) {
      return rejectWithValue(error.message)
    }
  }
)

export const getOneByName = createAsyncThunk(
  "pokemon/getOneByName",
  async ({name}, {rejectWithValue}) => {
    try {
      const url: string = `${import.meta.env.VITE_REACT_API_URL}/${name}`;

      const response: AxiosResponse = await axios.get(url);

      if (!response || response.status >= 400) {
        throw new Error();
      }

      const types = [];
      for (const type of response.data.types) {
        types.push(type.type.name);
      }

      const otherImages: string[] = [];
      const imageProperties: string[] = [
        "back_default",
        "back_female",
        "front_female"
      ];
      imageProperties.forEach(property => {
        if (response.data.sprites[property]) {
          otherImages.push(response.data.sprites[property]);
        }
      });

      const pokemonItemDetails: PokemonItemTypeDetails = {
        id: response.data.id,
        name: response.data.name,
        image: response.data.sprites.front_default,
        experience: response.data.base_experience,
        types,
        height: response.data.height,
        weight: response.data.weight,
        hp:  response.data.stats[0].base_stat,
        attack:  response.data.stats[1].base_stat,
        defense:  response.data.stats[2].base_stat,
        specialAttack:  response.data.stats[3].base_stat,
        specialDefense:  response.data.stats[4].base_stat,
        speed: response.data.stats[5].base_stat,
        otherImages
      }

      return pokemonItemDetails;
    } catch (error) {
      if (error.response && error.response.status === 404) {
        return rejectWithValue(`The pokemon with name "${name}" was not found`);
      }

      return rejectWithValue(error.message);
    }
  }
)

const pokemonSlice = createSlice({
  name: 'pokemon',
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(getAll.pending, (state) => {
        state.status = 'pending';
        state.error = null;
      })
      .addCase(getAll.fulfilled, (state, action: PayloadAction<PokemonType>) => {
        state.status = 'fulfilled';
        state.pokemon = action.payload;
      })
      .addCase(getAll.rejected, (state, action: PayloadAction<string>) => {
        state.status = 'rejected';
        state.error = action.payload;
      })

      .addCase(getOneByName.pending, (state) => {
        state.status = 'pending';
        state.error = null;
      })
      .addCase(getOneByName.fulfilled, (state, action: PayloadAction<PokemonItemTypeDetails>) => {
        state.status = 'fulfilled';
        state.selectedPokemonItem = action.payload;
      })
      .addCase(getOneByName.rejected, (state, action: PayloadAction<string>) => {
        state.status = 'rejected';
        state.error = action.payload;
      });
  },
});

export default pokemonSlice.reducer;