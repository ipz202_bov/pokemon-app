import {configureStore, combineReducers} from "@reduxjs/toolkit";

import pokemonReducer from "./reducers/pokemonSlice";

const rootReducer = combineReducers({
  pokemonReducer
});

export const store = configureStore({
  reducer: rootReducer
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;