export interface PokemonItemType {
  id: number,
  name: string,
  image: string,
  experience: number,
  types: string[]
}

export interface PokemonType {
  count: number | null,
  page: number,
  next: string | null,
  current: string,
  previous: string | null,
  pokemon: PokemonItemType[]
}

export interface PokemonItemTypeDetails extends PokemonItemType {
  height: number,
  weight: number,
  hp: number,
  attack: number,
  specialAttack: number,
  defense: number,
  specialDefense: number,
  speed: number,
  otherImages: string[]
}