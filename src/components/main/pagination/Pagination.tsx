import {FC} from "react";

import styles from "./pagination.module.scss";
import {useAppDispatch, useAppSelector} from "../../../hooks/redux";
import {getAll} from "../../../store/reducers/pokemonSlice";

const Pagination: FC = () => {
  const dispatch = useAppDispatch();
  const {pokemon} = useAppSelector(state => state.pokemonReducer);

  const pagesCount: number = Math.ceil(pokemon.count / parseInt(import.meta.env.VITE_POKEMON_COUNT_LIMIT));

  const firstPages: number[] = [1, 2];
  const lastPages: number[] = [pagesCount - 1, pagesCount];
  const middlePage: number = pokemon.page < Math.ceil(pagesCount / 2)
    ? pokemon.page + Math.ceil(pokemon.page / 2)
    : pokemon.page + Math.floor((pagesCount - 1 - pokemon.page) / 2);

  const previous = () => {
    dispatch(getAll({url: pokemon.previous, page: pokemon.page - 1}));
  }

  const next = () => {
    dispatch(getAll({url: pokemon.next, page: pokemon.page + 1}));
  }

  const goPage = (page) => {
    const limit: number = parseInt(import.meta.env.VITE_POKEMON_COUNT_LIMIT);
    const offset: number = limit * page - limit;

    const url: string = `${import.meta.env.VITE_REACT_API_URL}?offset=${offset}&limit=${limit}`

    dispatch(getAll({url, page}));
  }

  return (
    <div className={styles.container}>
      {pagesCount > 0 &&
        <div className={styles.wrapper}>
          {pokemon.previous &&
            <button
              className={styles.previous_button}
              onClick={previous}
            >
              {"<"}
            </button>
          }

          <div className="flex items-end gap-2 flex-wrap">
            {firstPages.map((page) => (
              <button
                key={page}
                className={page === pokemon.page ? styles.page__selected : styles.page}
                onClick={() => goPage(page)}
              >
                {page}
              </button>
            ))}

            {pokemon.page > firstPages[firstPages.length - 1] && pokemon.page < lastPages[0]
              && <>
                {pokemon.page - firstPages[firstPages.length - 1] > 1 &&
                  <span>...</span>
                }

                <button
                  className={styles.page__selected}
                  onClick={() => goPage(pokemon.page)}
                >
                  {pokemon.page}
                </button>

                {pokemon.page + 1 < lastPages[0] &&
                  <button
                    className={styles.page}
                    onClick={() => goPage(pokemon.page + 1)}
                  >
                    {pokemon.page + 1}
                  </button>
                }

                {middlePage - pokemon.page > 2 &&
                  <span>...</span>
                }

                {middlePage < lastPages[0] && middlePage > pokemon.page + 1 &&
                  <button
                    className={styles.page}
                    onClick={() => goPage(middlePage)}
                  >
                    {middlePage}
                  </button>
                }
              </>
            }

            {lastPages[0] - middlePage !== 1 &&
              <span>...</span>
            }
            {lastPages.map((page) => (
              <button
                key={page}
                className={page === pokemon.page ? styles.page__selected : styles.page}
                onClick={() => goPage(page)}
              >
                {page}
              </button>
            ))}
          </div>

          {pokemon.next &&
            <button
              className={styles.next_button}
              onClick={next}
            >
              {">"}
            </button>
          }
        </div>
      }
    </div>
  );
};

export default Pagination;
