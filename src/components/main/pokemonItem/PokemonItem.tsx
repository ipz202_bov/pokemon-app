import {FC} from "react";
import {Link} from "react-router-dom";

import {PokemonItemType} from "../../../types/pokemon";
import {MAIN_ROUTE} from "../../../../routes";
import getClassByType from "../../../helpers/getClassByType";

import styles from "./pokemonItem.module.scss";

interface PokemonItemProps {
  item: PokemonItemType
}

const PokemonItem: FC = (props: PokemonItemProps) => {
  return (
    <Link to={MAIN_ROUTE + props.item.name} className={styles.container}>
      <div className={styles.card}>
        <img className={styles.card__image} src={props.item.image} alt={props.item.name}/>

        <div className={styles.card__props}>
          <p className={styles.card__props__id}>#{props.item.id}</p>
          <p className={styles.card__props__experience}>Exp: {props.item.experience}</p>
        </div>

        <h4 className={styles.card__name}>{props.item.name}</h4>
        <p className={styles.card__types}>
          {props.item.types.map(type =>
            <span key={type} className={`${getClassByType(type)} ${styles.card__types__item}`}>{type}</span>
          )}
        </p>
      </div>
    </Link>
  );
};

export default PokemonItem;
