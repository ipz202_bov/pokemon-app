import {FC, useState} from "react";
import {useNavigate} from 'react-router-dom';

import {MAIN_ROUTE} from "../../../../routes";
import styles from "./searchBar.module.scss";

const SearchBar: FC = () => {
  const navigate = useNavigate()

  const [name, setName] = useState<string>('');

  const handleSearch = () => {
    if (!name)
      return;

    navigate(MAIN_ROUTE + name);

    setName('');
  }

  return (
    <div className={styles.container}>
      <form onSubmit={handleSearch} className={styles.wrapper}>
        <input
          onChange={e => setName(e.target.value)}
          value={name}
          className={styles.input} type="text" placeholder="pikachu"
        />
        <button type="submit" className={styles.button}>Search</button>
      </form>
    </div>
  );
};

export default SearchBar;
