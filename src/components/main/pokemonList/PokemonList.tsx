import {useAppSelector} from "../../../hooks/redux";
import {FC} from "react";

import styles from "./pokemonList.module.scss";
import PokemonItem from "../pokemonItem/PokemonItem";

const PokemonList: FC = () => {
  const {pokemon} = useAppSelector(state => state.pokemonReducer);

  return (
    <div className={styles.container}>
      {pokemon.pokemon.map(item =>
        <PokemonItem key={item.id} item={item}/>
      )}
    </div>
  );
};

export default PokemonList;
