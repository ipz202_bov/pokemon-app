import {FC, useState} from "react";
import {useNavigate} from 'react-router-dom';
import {useAppDispatch} from "../../hooks/redux";

import getClassByType from "../../helpers/getClassByType";

import {MAIN_ROUTE} from "../../../routes";
import {PokemonItemTypeDetails} from "../../types/pokemon";
import styles from "./pokemonItemDetailsCard.module.scss";

interface PokemonItemDetailsCardProps {
  itemDetails: PokemonItemTypeDetails;
}

const PokemonItemDetailsCard: FC = (props: PokemonItemDetailsCardProps) => {
  const navigate = useNavigate();
  const selectedPokemonItem: PokemonItemTypeDetails = props.itemDetails;

  const [currentMainImage, setCurrentMainImage] = useState<string>(selectedPokemonItem.image);
  const [otherImages, setOtherImages] = useState<string[]>(selectedPokemonItem.otherImages);

  const changeCurrentMainImage = (newImage: string) => {
    const replacedIndex = otherImages.findIndex(element => element === newImage);
    if (replacedIndex !== -1) {
      const updatedOtherImages = [...otherImages];
      updatedOtherImages[replacedIndex] = currentMainImage;

      setOtherImages(updatedOtherImages);
    }

    setCurrentMainImage(newImage);
  };

  const home = () => {
    navigate(MAIN_ROUTE);
  }

  return (
    <>
      {selectedPokemonItem &&
        <div className={styles.container}>
          <div className={styles.left}>
            <div className="flex flex-col justify-center items-center">
              <img
                className={`${styles.mainImage} ${styles.fadeIn}`}
                src={currentMainImage}
                alt={selectedPokemonItem.name}
              />

              <div className={styles.otherImages}>
                {otherImages.map(image =>
                  <img
                    key={image}
                    src={image}
                    alt="Small Image"
                    className={styles.otherImages__item}
                    onClick={() => changeCurrentMainImage(image)}
                  />
                )}
              </div>
            </div>
          </div>

          <div className={styles.right}>
            <div className="flex items-center">
              <p className={styles.id}>#{selectedPokemonItem.id}</p>

              <h2 className={styles.name}>{selectedPokemonItem.name}</h2>

              <p className={styles.types}>
                {selectedPokemonItem.types.map(type =>
                  <span key={type} className={`${getClassByType(type)} ${styles.types__item}`}>{type}</span>
                )}
              </p>
            </div>

            <table className={styles.table}>
              <tbody>
              <tr>
                <td className={styles.table__cellKey}>Experience</td>
                <td className={styles.table__cellValue}>{selectedPokemonItem.experience}</td>
              </tr>

              <tr>
                <td className={styles.table__cellKey}>Hp</td>
                <td className={styles.table__cellValue}>{selectedPokemonItem.hp}</td>
              </tr>

              <tr>
                <td className={styles.table__cellKey}>Height</td>
                <td className={styles.table__cellValue}>{selectedPokemonItem.height}</td>
              </tr>

              <tr>
                <td className={styles.table__cellKey}>Weight</td>
                <td className={styles.table__cellValue}>{selectedPokemonItem.weight}</td>
              </tr>

              <tr>
                <td className={styles.table__cellKey}>Attack</td>
                <td className={styles.table__cellValue}>{selectedPokemonItem.attack}</td>
              </tr>

              <tr>
                <td className={styles.table__cellKey}>Defense</td>
                <td className={styles.table__cellValue}>{selectedPokemonItem.defense}</td>
              </tr>

              <tr>
                <td className={styles.table__cellKey}>Special attack</td>
                <td className={styles.table__cellValue}>{selectedPokemonItem.specialAttack}</td>
              </tr>

              <tr>
                <td className={styles.table__cellKey}>Special defense</td>
                <td className={styles.table__cellValue}>{selectedPokemonItem.specialDefense}</td>
              </tr>

              <tr>
                <td className={styles.table__cellKey}>Speed</td>
                <td className={styles.table__cellValue}>{selectedPokemonItem.speed}</td>
              </tr>
              </tbody>
            </table>

            <div className="flex justify-end">
              <button
                className={styles.button}
                onClick={home}
              >
                Back home
              </button>
            </div>
          </div>
        </div>
      }
    </>
  );
};

export default PokemonItemDetailsCard;
