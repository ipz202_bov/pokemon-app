import {FC} from "react";

import styles from "./loader.module.scss"

const Loader: FC = () => {
  return (
    <div className={styles.container}>
      <div className={styles.wrapper}></div>
    </div>
  );
};

export default Loader;