import {FC} from "react";

import styles from "./error.module.scss";

interface ErrorProps {
  message: string;
}

const Error: FC = (props: ErrorProps) => {
  return (
    <div className={styles.container}>
      <div className={styles.message}>
        {props.message}
      </div>
    </div>
  );
};

export default Error;
