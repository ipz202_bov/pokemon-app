import {FC} from "react";
import {Link} from 'react-router-dom';

import styles from "./header.module.scss";
import {MAIN_ROUTE} from "../../../../routes";

const Header: FC = () => {
  return (
    <header className={styles.container}>
      <Link to={MAIN_ROUTE} className={styles.logo}>
        <img
          src="https://freepngimg.com/thumb/pokemon/6-2-pokemon-png-hd.png" alt="logo"
          className={styles.logo__image}
        />

        <span className={styles.logo__text}>Pokemon App</span>
      </Link>
    </header>
  );
};

export default Header;
