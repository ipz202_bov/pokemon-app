import {FC, useEffect} from "react";
import {useAppDispatch, useAppSelector} from "../hooks/redux";

import {getAll} from "../store/reducers/pokemonSlice";
import Loader from "../components/shared/loader/Loader";
import SearchBar from "../components/main/searchBar/SearchBar";
import PokemonList from "../components/main/pokemonList/PokemonList";
import Pagination from "../components/main/pagination/Pagination";
import Error from "../components/shared/error/Error";

const Main: FC = () => {
  const dispatch = useAppDispatch();
  const {status, error, pokemon} = useAppSelector(state => state.pokemonReducer);

  useEffect(() => {
    dispatch(getAll({url: pokemon.current, page: pokemon.page}))
  }, []);

  return (
    <>
      {error || status === 'rejected'
        ? <Error message={error}/>
        : <>
            <SearchBar/>

            {status === 'pending' || status === 'idle'
              ? <Loader/>
              : <>
                <PokemonList/>
                <Pagination/>
                </>
            }
          </>
      }
    </>
  );
};

export default Main;
