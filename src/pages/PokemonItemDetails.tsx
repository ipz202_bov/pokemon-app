import {FC, useEffect} from "react";
import {useParams, useNavigate} from 'react-router-dom';
import {useAppDispatch, useAppSelector} from "../hooks/redux";

import {getOneByName} from "../store/reducers/pokemonSlice";
import Error from "../components/shared/error/Error";
import Loader from "../components/shared/loader/Loader";
import PokemonItemDetailsCard from "../components/pokemonItemDetails/PokemonItemDetailsCard";
import {MAIN_ROUTE} from "../../routes";

const PokemonItemDetails: FC = () => {
  const dispatch = useAppDispatch();
  const {status, error, selectedPokemonItem} = useAppSelector(state => state.pokemonReducer);

  const navigate = useNavigate();

  const {name} = useParams();

  useEffect(() => {
    dispatch(getOneByName({name}));
  }, []);

  const home = () => {
    navigate(MAIN_ROUTE);
  }

  return (
    <>
      {error || status === 'rejected'
        ? <>
            <Error message={error}/>

          <div className="flex justify-center">
            <button
              className="mt-8 p-2 bg-gray-800 text-white rounded hover:bg-gray-500"
              onClick={home}
            >
              Back home
            </button>
          </div>
          </>
        : <>
          {status === 'pending' || status === 'idle' || !selectedPokemonItem
            ? <Loader/>
            : <>
                <PokemonItemDetailsCard itemDetails={selectedPokemonItem}/>
              </>
          }
        </>
      }
    </>
  );
};

export default PokemonItemDetails;
