import {FC} from "react";
import {Routes, Route} from "react-router-dom";

import {routes} from "../routes";
import Main from "./pages/Main";
import Header from "./components/shared/header/Header";

const App: FC = () => {
    return (
      <>
        <Header/>

        <Routes>
          {routes.map(item =>
            <Route key={item.path} path={item.path} element={<item.component/>}/>
          )}
          <Route path="*" element={<Main/>}/>
        </Routes>
      </>
  )
}

export default App
