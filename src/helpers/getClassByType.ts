export default function (type: string) {
  switch (type) {
    case 'poison':
      return 'bg-purple-400';
    case 'ground':
      return 'bg-yellow-400';
    case 'rock':
      return 'bg-yellow-700';
    case 'bug':
      return 'bg-green-500';
    case 'ghost':
      return 'bg-indigo-700';
    case 'steel':
      return 'bg-gray-400';
    case 'fire':
      return 'bg-orange-500';
    case 'water':
      return 'bg-blue-500';
    case 'grass':
      return 'bg-green-500';
    case 'electric':
      return 'bg-yellow-300';
    case 'psychic':
      return 'bg-pink-400';
    case 'ice':
      return 'bg-blue-200';
    case 'dragon':
      return 'bg-indigo-900';
    case 'dark':
      return 'bg-gray-800';
    case 'fairy':
      return 'bg-pink-300';
    case 'unknown':
      return 'bg-gray-300';
    case 'shadow':
      return 'bg-gray-900';
    default:
      return 'bg-gray-500';
  }
}